import gulp from 'gulp';

import browserify from 'browserify';
import babelify from 'babelify';
import source from 'vinyl-source-stream';

import sass from 'gulp-sass';
import postcss from 'gulp-postcss';
import streamify from 'gulp-streamify';
import uglify from 'gulp-uglify';
import cssnext from 'postcss-cssnext';

const paths = {
  scripts: {
    src: 'src/js/',
    dist: 'extension/js/build/'
  },
  styles: {
    sass: 'src/sass/',
    css: 'extension/css/'
  }
};

// ===================================
// JavaScript ES2015-2016
// ===================================
function buildContentScript() {
  return browserify(`${paths.scripts.src}content_scripts.js`)
    .transform('babelify', { presets: [ 'es2015', 'es2016' ] })
    .bundle()
    .pipe(source('content_scripts.js'));
}

function buildPopup() {
  return browserify(`${paths.scripts.src}popup.js`)
    .transform('babelify', { presets: [ 'es2015', 'es2016', 'react' ] })
    .bundle()
    .pipe(source('popup.js'));
}

gulp.task('dev_build', () => {
  buildContentScript()
    .pipe(gulp.dest(paths.scripts.dist));

  buildPopup()
    .pipe(gulp.dest(paths.scripts.dist));
});

gulp.task('build', () => {
  process.env.NODE_ENV = 'production';

  buildContentScript()
    .pipe(streamify(uglify()))
    .pipe(gulp.dest(paths.scripts.dist));

  buildPopup()
    .pipe(streamify(uglify()))
    .pipe(gulp.dest(paths.scripts.dist));
});

// ===================================
// Sass -> CSS
// ===================================
gulp.task('sass', () => {
  const processors = [ cssnext() ];
  gulp.src(`${paths.styles.sass}**/*.scss`)
    .pipe(sass())
    .pipe(postcss(processors))
    .pipe(gulp.dest(paths.styles.css));
});

// ===================================
// Watch
// ===================================
gulp.task('watch', () => {
  gulp.watch(`${paths.scripts.src}**/*.js`, [ 'dev_build' ]);
  gulp.watch(`${paths.styles.sass}**/*.scss`, [ 'sass' ]);
});
