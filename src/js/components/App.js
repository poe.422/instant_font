'use strict';

import React from 'react';

import Categories from './Categories';
import FontSelect from './FontSelect';
import Orders from './Orders';

export default class App extends React.Component {

  constructor(prop) {
    super(prop);

    const fonts = require('../data/fonts.json');

    this.state = {
      fontsList: fonts,
      font: fonts[0].family,
      order: 'popularity',
      categories: [
        { name: 'Serif', value: true},
        { name: 'Sans Serif', value: true},
        { name: 'Monospace', value: true},
        { name: 'Handwriting', value: true},
        { name: 'Display', value: true}
      ],
      selector: 'body'
    };
  }

  onFontChange(event) {
    this.setState({ font: event.target.value });
  }

  onSelectorChange(event) {
    this.setState({ selector: event.target.value });
  }

  onOrderChange(event) {
    this.setState({ order: event.target.value });
  }

  onCheckBoxChange(event) {
    const categories = this.state.categories.map(category => {
      if (category.name === event.target.value) {
        category.value = event.target.checked;
      }
      return category;
    });
    this.setState({ categories });
  }

  onSwitchClick() {
    this.switchFont(this.state.font, this.state.selector);
  }

  onResetClick() {
    this.resetFont();
  }

  switchFont(fontName, selector) {
    chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
      chrome.tabs.sendMessage(tabs[0].id, {
        type: "SWITCH_FONT",
        fontName: fontName,
        selector: selector
      });
    });
  }

  resetFont() {
    chrome.tabs.query({ active: true, currentWindow: true }, tabs => {
      chrome.tabs.sendMessage(tabs[0].id, {
        type: "RESET_FONT"
      });
    });
  }

  render() {
    return (
      <div className="container">
        <h1>Instant Font</h1>
        <div className="form-group">
          <h2>Search Font</h2>
          <h3>Categories</h3>
          <div className="input-area">
            <Categories items={this.state.categories}
                        onChange={this.onCheckBoxChange.bind(this)} />
          </div>
          <h3>Sort</h3>
          <div className="input-area">
            <Orders checkedItem={this.state.order}
                    onChange={this.onOrderChange.bind(this)} />
          </div>
          <div className="input-area">
            <FontSelect fontsList={this.state.fontsList}
                        order={this.state.order}
                        categories={this.state.categories}
                        font={this.state.font}
                        onChange={this.onFontChange.bind(this)} />
          </div>
        </div>
        <div className="form-group">
          <h2>Selector</h2>
          <div className="input-area">
            <input type="text"
                   value={this.state.selector}
                   onChange={this.onSelectorChange.bind(this)} />
          </div>
        </div>
        <div className="buttons float-box">
          <button className="btn btn--primary pull-left"
                  onClick={this.onSwitchClick.bind(this)}>Switch</button>
          <button className="btn btn--flat pull-right"
                  onClick={this.onResetClick.bind(this)}>Reset</button>
        </div>
      </div>
    );
  }

}
