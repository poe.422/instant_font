'use strict';

import React from 'react';
import _ from 'lodash';

export default class FontSelect extends React.Component {
  render() {
    const renderFontsOptions = () => {
      return _.sortBy(this.props.fontsList, [this.props.order])
        .filter(font => {
          return this.props.categories.filter(category => {
            return category.name === font.category;
          })[0].value;
        })
        .map(font => {
          return <option key={font.family} value={font.family}>{font.family}</option>
        });
    };

    return (
      <select name="fonts" id="fonts"
              value={this.props.font}
              onChange={this.props.onChange}>
        {renderFontsOptions()}
      </select>
    );
  }
}
