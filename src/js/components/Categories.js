'use strict';

import React from 'react';

import CategoryCheckBox from './CategoryCheckBox';

export default class Categories extends React.Component {
  render() {
    const categories = this.props.items.map(category => {
      return <CategoryCheckBox key={category.name}
                               value={category.name}
                               checked={category.value}
                               onChange={this.props.onChange}/>;
    });

    return (
      <div>{categories}</div>
    );
  }
}
