'use strict';

import React from 'react';

export default class Orders extends React.Component {
  render() {
    return (
      <div>
        <label htmlFor="order1">
          <input type="radio" name="order" id="order1"
                 value="popularity"
                 checked={this.props.checkedItem === "popularity"}
                 onChange={this.props.onChange} />
          popularity
        </label>
        <label htmlFor="order2">
          <input type="radio" name="order" id="order2"
                 value="family"
                 checked={this.props.checkedItem === "family"}
                 onChange={this.props.onChange} />
          alphabetical
        </label>
      </div>
    );
  }
}
