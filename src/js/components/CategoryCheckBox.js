'use strict';

import React from 'react';

export default class CategoryCheckBox extends React.Component {
  render() {
    return (
      <label htmlFor={this.props.value}>
        <input type="checkbox" name="categories" id={this.props.value}
               value={this.props.value}
               checked={this.props.checked}
               onChange={this.props.onChange} />{this.props.value}
      </label>
    );
  }
}
